from kivy import require
require("2.1.0")

from kivy.uix.image import AsyncImage
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.button import Button
from kivy.network.urlrequest import UrlRequest
from kivy.uix.switch import Switch
from kivy_garden.mapview import MapView, MapMarker

from components.popups import InfoPopupContent, TextInputPopupContent, AboutPopup
from components.layouts import RoundedCornerLayout

import datetime

class MapScreenContent(MapView):
    def __init__(self, zoom, app, c_or_f, store, key, colour, secondaryColour):
        super().__init__()

        self.aboutButton = None
        self.secondaryColour = secondaryColour
        self.detailsButton,self.settingsButton,self.lowerLeftFloat,self.leftFloat = None,None,None,None
        self.bookmark,self.bookmarkTitle,self.description,self.weatherImg = None,None,None,None
        self.minTemp,self.maxTemp,self.currentTemp,self.placename,self.switch= None,None,None,None,None
        self.label,self.marker,self.curData,self.stopbookmarking = None,None,None,None
        self.bookmarkers,self.bookmarkerbuttons,self.removebookmarkerbuttons = {},{},{}
        self.zoom,self.app,self.c_or_f,self.store,self.key,self.colour = zoom,app,c_or_f,store,key,colour
        self.startIndex = 1
        self.futureData = None



    def on_size(self, instance, size):
        try:
            positions = self.store.get('bookmarkmarkerspos')['val']
            names = self.store.get('bookmarknames')['val']
            self.refresh(positions,names,True)
        except KeyError:
            print("There is no data in the jsonstore, opening ui with no data")
            try:
                self.refresh([], [])
            except AttributeError:
                print("__init__ after super() is not yet complete")


    def refresh(self,positions,names, doMarkers=False):
        scolour =tuple(int(str(self.secondaryColour).strip("#")[i:i + 2], 16) for i in (0, 2, 4, 6))
        degreessign = "º?"
        if self.c_or_f == "c":
            degreessign = "ºC"
        elif self.c_or_f == "f":
            degreessign = "ºF"
        switchval = False
        try:
            self.lowerLeftFloat.clear_widgets()
            self.leftFloat.clear_widgets()
            switchval = self.switch.active
        except:
            pass
        self.remove_widget(self.lowerLeftFloat)
        self.remove_widget(self.leftFloat)
        self.remove_widget(self.settingsButton)
        self.remove_widget(self.detailsButton)
        self.remove_widget(self.aboutButton)

        self.detailsButton, self.settingsButton, self.lowerLeftFloat, self.leftFloat = None, None, None, None
        self.bookmark, self.bookmarkTitle, self.description, self.weatherImg = None, None, None, None
        self.minTemp, self.maxTemp, self.currentTemp, self.placename, self.switch = None, None, None, None, None
        self.label, self.curData, self.stopbookmarking = None, None, None
        self.bookmarkers, self.bookmarkerbuttons, self.removebookmarkerbuttons = {}, {}, {}

        self.label = Label(text="Weather:", pos_hint={"center_x": 0.5, "center_y": 0.9})
        self.switch = Switch(active=switchval, pos_hint={"center_x": 0.5, "center_y": 0.8})
        self.placename = Label(text="Place", pos_hint={"center_x": 0.5, "center_y": 0.7})
        self.currentTemp = Label(text="[b]XX" + degreessign + "[/b]", font_size=round(self.size[1] / 20),
                                 pos_hint={"center_x": 0.5, "center_y": 0.6},
                                 markup=True)
        self.maxTemp = Label(text="[color=ff0000]XX" + degreessign + "[/color]", font_size=round(self.size[1] / 30),
                             pos_hint={"center_x": 0.75, "center_y": 0.5}, markup=True)
        self.minTemp = Label(text="[color=0000ff]XX" + degreessign + "[/color]", font_size=round(self.size[1] / 30),
                             pos_hint={"center_x": 0.25, "center_y": 0.5}, markup=True)
        self.weatherImg = AsyncImage(source="https://www.weatherbit.io/static/img/icons/c04d.png",
                                     pos_hint={"center_x": 0.5, "center_y": 0.34})
        self.description = Label(text="Description",
                                 pos_hint={"center_x": 0.5, "center_y": 0.17})

        self.bookmarkTitle = Label(text="Bookmarks:", pos_hint={"center_x": 0.5, "center_y": 0.9})
        self.bookmark = Button(text="Bookmark", pos_hint={"center_x": 0.5, "center_y": 0.8}, size_hint=(0.9, 0.1))
        self.bookmark.bind(on_release=self.createBookmark)

        self.leftFloat = RoundedCornerLayout(ssize=(self.size[0] * .2, self.size[1] * .47), posaddy=self.size[1] * .47 + 15,
                                             roundedness=self.size[0] // 40,colour=self.colour)
        self.lowerLeftFloat = RoundedCornerLayout(ssize=(self.size[0] * .2, self.size[1] * .47), posaddy=0,
                                                  roundedness=self.size[0] // 40,colour=self.colour)

        self.leftFloat.add_widget(self.switch)
        self.leftFloat.add_widget(self.minTemp)
        self.leftFloat.add_widget(self.label)
        self.leftFloat.add_widget(self.placename)
        self.leftFloat.add_widget(self.weatherImg)
        self.leftFloat.add_widget(self.currentTemp)
        self.leftFloat.add_widget(self.maxTemp)
        self.leftFloat.add_widget(self.description)

        self.lowerLeftFloat.add_widget(self.bookmark)
        self.lowerLeftFloat.add_widget(self.bookmarkTitle)

        self.add_widget(self.leftFloat)
        self.add_widget(self.lowerLeftFloat)
        try:
            self.getWeather(self.marker.lat,self.marker.lon)
        except:
            pass
        def settingsTouchDown(e, touch):
            if e.collide_point(touch.pos[0],touch.pos[1]):
                e.color = (1,1,1,eval("0x"+str(self.colour[-2:])))

        def settingsTouchUp(e,touch):
            if e.collide_point(touch.pos[0],touch.pos[1]) and e.state:
                self.app.open_settings()

            e.color = self.colour

        def detailsTouchDown(e,touch):
            if e.collide_point(touch.pos[0], touch.pos[1]):
                e.color = (1,1,1,eval("0x"+str(self.colour[-2:])))
        def detailsTouchUp(e,touch):
            if e.collide_point(touch.pos[0], touch.pos[1]) and e.state:
                if self.curData is not None:
                    self.app.forecast.setLatLon(self.curData["lat"],self.curData["lon"])
                elif self.marker is not None:
                    self.app.forecast.setLatLon(self.marker.lat, self.marker.lon)
                self.app.screenManager.switch_to(self.app.forecastscreen,direction='left')
            e.color = self.colour

        def aboutTouchDown(e,touch):
            if e.collide_point(touch.pos[0], touch.pos[1]):
                e.color = (1,1,1,eval("0x"+str(self.colour[-2:])))
        def aboutTouchUp(e,touch):
            if e.collide_point(touch.pos[0], touch.pos[1]) and e.state:
                AboutPopup((self.size[0] *11 / 20, self.size[1]*12 / 20))
            e.color = self.colour
        self.settingsButton = Button(background_normal='',
                                     pos=(self.size[0] - self.size[0]*0.1, self.size[1] - self.size[1]*0.133),
                                     background_down='',
                                     text="~",
                                     font_name="./dripicons-v2.ttf",
                                     font_size=max(11, min(0.09*self.size[1], 60)),
                                     color=self.colour,
                                     background_color=(0,0,0,0),
                                     on_touch_down= settingsTouchDown,
                                     on_touch_up = settingsTouchUp)
        self.detailsButton = Button(background_normal='',
                                     pos=(self.size[0] - self.size[0]*0.1, self.size[1] - self.size[1]*0.2416),
                                     background_down='',
                                     text="\ue020",
                                     font_name="./dripicons-v2.ttf",
                                     font_size=max(11, min(0.09 * self.size[1], 60)),
                                     color=self.colour,
                                     background_color=(0, 0, 0, 0),
                                     on_touch_down=detailsTouchDown,
                                     on_touch_up=detailsTouchUp)
        self.aboutButton = Button(background_normal='',
                                    pos=(self.size[0] - self.size[0]*0.1, 0),
                                    background_down='',
                                    text="\ue009",
                                    font_name="./dripicons-v2.ttf",
                                    font_size=max(11, min(0.09 * self.size[1], 60)),
                                    color=self.colour,
                                    background_color=(0, 0, 0, 0),
                                    on_touch_down=aboutTouchDown,
                                    on_touch_up=aboutTouchUp)


        self.add_widget(self.settingsButton)
        self.add_widget(self.detailsButton)
        self.add_widget(self.aboutButton)
        self.center_on(0, 0)
        self.bookmarkerbuttons = {}
        self.removebookmarkerbuttons = {}
        for i in range(0, len(positions)):
            name = names[i]

            newbookmarkbutton = Button(text=name, size_hint=(0.65, 0.1),
                                       pos_hint={"center_x": 0.4,
                                                 "center_y": 0.65 - (0.1 * len(self.bookmarkerbuttons))})
            newbookmarkbutton.bind(on_release=lambda e, n=name: self.gotoBookmark(n))
            self.lowerLeftFloat.add_widget(newbookmarkbutton)
            self.bookmarkerbuttons[name] = newbookmarkbutton

            newremovebookmarkbutton = Button(text="X", size_hint=(0.2, 0.1),
                                             pos_hint={"center_x": 0.83,
                                                       "center_y": 0.65 - (
                                                               0.1 * len(self.bookmarkerbuttons)) + 0.1})
            newremovebookmarkbutton.bind(on_release=lambda e, n=name: self.deleteBookmark(n))
            self.lowerLeftFloat.add_widget(newremovebookmarkbutton)
            self.removebookmarkerbuttons[name] = newremovebookmarkbutton
            if doMarkers:
                newmarker = MapMarker(lat=positions[i][0], lon=positions[i][1], source="./images/bookmarker.png")
                self.add_marker(newmarker)
                self.bookmarkers[name] = newmarker


    def setColourUnits(self,colour=None,c_or_f=None,secondaryColour=None):
        if colour is not None:
            self.colour = colour
            rgba = tuple(int(str(self.colour).strip("#")[i:i + 2], 16) for i in (0, 2, 4, 6))
            print(rgba)
        if c_or_f is not None:
            self.c_or_f = c_or_f
        if secondaryColour is not None:
            self.secondaryColour = secondaryColour

    def setLatLon(self, lat, lon):
        if self.marker is not None:
            self.marker.lat = lat
            self.marker.lon = lon
            self.remove_marker(self.marker)
            self.add_marker(self.marker)
        else:
            self.marker = MapMarker(lat=lat, lon=lon, source='./images/marker.png')
            self.add_marker(self.marker)
        self.getWeather(lat,lon)


    def getWeather(self, lat, lon):
        def setStuff(req, result):
            degreessign = "º?"
            if self.c_or_f == "c":
                degreessign = "ºC"
            elif self.c_or_f == "f":
                degreessign = "ºF"

            self.curData = result["data"][0]
            self.placename.text = self.curData["city_name"]
            self.currentTemp.text = str(self.curData["temp"])+ degreessign
            self.weatherImg.source = "https://www.weatherbit.io/static/img/icons/" + self.curData["weather"]["icon"] + ".png"
            self.description.text = self.curData["weather"]["description"]
        def setStuffFuture(req,result):
            degreessign = "º?"
            if self.c_or_f == "c":
                degreessign = "ºC"
            elif self.c_or_f == "f":
                degreessign = "ºF"

            for r in result["data"]:
                if r["datetime"] == datetime.date.today().strftime("%Y-%m-%d"):
                    self.startIndex = result["data"].index(r)
                    break

            self.futureData = result

            self.minTemp.text = "[color=0000ff]" + str(self.futureData["data"][self.startIndex]["min_temp"]) + degreessign + "[/color]"
            self.maxTemp.text = "[color=ff0000]" + str(self.futureData["data"][self.startIndex]["max_temp"]) + degreessign + "[/color]"

        if self.c_or_f == "c":
            UrlRequest(
                'https://api.weatherbit.io/v2.0/current?key=' + self.key + '&lat=' + str(lat) + '&lon=' + str(lon) + "&units=m",
                setStuff)
            UrlRequest(
                'https://api.weatherbit.io/v2.0/forecast/daily?key=' + self.key + '&lat=' + str(lat) + '&lon=' + str(lon) + "&units=m",
                setStuffFuture)
        else:
            UrlRequest(
                'https://api.weatherbit.io/v2.0/current?key=' + self.key + '&lat=' + str(lat) + '&lon=' + str(
                    lon) + "&units=i",
                setStuff)
            UrlRequest(
                'https://api.weatherbit.io/v2.0/forecast/daily?key=' + self.key + '&lat=' + str(lat) + '&lon=' + str(
                    lon) + "&units=i",
                setStuffFuture)


    def createBookmark(self, event):
        if self.marker is not None:
            self.stopbookmarking = False
            latlons = []
            for value in self.bookmarkers.values():
                latlons.append((value.lat, value.lon))
            if len(self.bookmarkers) < 6:
                if not (self.marker.lat, self.marker.lon) in latlons:
                    name = "Bookmark " + str(len(self.bookmarkers) + 1)
                    if self.placename.text != "" and self.placename.text is not None:
                        name = self.placename.text

                    show = TextInputPopupContent("Bookmark name:", name)
                    popupWindow = Popup(title="New Bookmark", content=show, size_hint=(None, None),
                                        size=(self.size[0] * .375, self.size[1] * .33),
                                        auto_dismiss=False)

                    def close_save(e):
                        global name
                        name = popupWindow.content.textinput.text
                        popupWindow.dismiss()
                        if name in self.bookmarkerbuttons.keys():
                            def rerun(e):
                                popupWindow2.dismiss()
                                self.createBookmark(e)

                            show = InfoPopupContent(
                                "That location is not bookmarked, but another \nbookmark already has the same name.")
                            popupWindow2 = Popup(title="Sorry", content=show, size_hint=(None, None),
                                                 size=(self.size[0] * .43, self.size[1] * .25), auto_dismiss=False)
                            popupWindow2.content.button.bind(on_release=rerun)
                            popupWindow2.open()
                            return

                        newmarker = MapMarker(lat=self.marker.lat, lon=self.marker.lon, source="./images/bookmarker.png")
                        self.add_marker(newmarker)
                        self.remove_marker(self.marker)
                        self.bookmarkers[name] = newmarker

                        newbookmarkbutton = Button(text=name, size_hint=(0.65, 0.1),
                                                   pos_hint={"center_x": 0.4,
                                                             "center_y": 0.65 - (0.1 * len(self.bookmarkerbuttons))})
                        newbookmarkbutton.bind(on_release=lambda e: self.gotoBookmark(newbookmarkbutton.text))
                        self.lowerLeftFloat.add_widget(newbookmarkbutton)
                        self.bookmarkerbuttons[name] = newbookmarkbutton

                        newremovebookmarkbutton = Button(text="X", size_hint=(0.2, 0.1),
                                                         pos_hint={"center_x": 0.83,
                                                                   "center_y": 0.65 - (
                                                                           0.1 * len(self.bookmarkerbuttons)) + 0.1})
                        newremovebookmarkbutton.bind(on_release=lambda e: self.deleteBookmark(newbookmarkbutton.text))
                        self.lowerLeftFloat.add_widget(newremovebookmarkbutton)
                        self.removebookmarkerbuttons[name] = newremovebookmarkbutton

                    popupWindow.content.okay.bind(on_release=close_save)
                    popupWindow.content.cancel.bind(on_release=popupWindow.dismiss)
                    popupWindow.content.textinput.bind(on_text_validate=close_save)
                    popupWindow.open()

                else:
                    show = InfoPopupContent("That location is already bookmarked.")
                    popupWindow = Popup(title="Sorry", content=show, size_hint=(None, None),
                                        size=(self.size[0] * .375, self.size[1] * .25), auto_dismiss=False)
                    popupWindow.content.button.bind(on_release=popupWindow.dismiss)
                    popupWindow.open()
            else:
                show = InfoPopupContent("You already have the maximum number of bookmarks.")
                popupWindow = Popup(title="Sorry", content=show, size_hint=(None, None),
                                    size=(self.size[0] * .5, self.size[1] * .25),
                                    auto_dismiss=False)
                popupWindow.content.button.bind(on_release=popupWindow.dismiss)
                popupWindow.open()
        else:
            show = InfoPopupContent("You have no location selected.")
            popupWindow = Popup(title="Sorry", content=show, size_hint=(None, None),
                                size=(self.size[0] * .375, self.size[1] * .25), auto_dismiss=False)
            popupWindow.content.button.bind(on_release=popupWindow.dismiss)
            popupWindow.open()

    def gotoBookmark(self, name):
        if self.zoom < 8:
            self.zoom = 8
        self.remove_marker(self.marker)
        self.center_on(self.bookmarkers[name].lat, self.bookmarkers[name].lon)
        self.getWeather(self.lat, self.lon)

    def deleteBookmark(self, name):
        self.remove_marker(self.bookmarkers[name])
        self.lowerLeftFloat.remove_widget(self.bookmarkerbuttons[name])
        self.lowerLeftFloat.remove_widget(self.removebookmarkerbuttons[name])
        l = self.bookmarkerbuttons.values()
        bookmarker = []
        for b in l:
            bookmarker.append(b)
        del self.bookmarkers[name]
        del self.bookmarkerbuttons[name]
        del self.removebookmarkerbuttons[name]
        positions = []
        for b in self.bookmarkers.values():
            positions.append((b.lat, b.lon))
            self.remove_marker(b)
        names = []
        for n in self.bookmarkers.keys():
            names.append(n)
        for j in self.bookmarkerbuttons.values():
            self.lowerLeftFloat.remove_widget(j)
        for k in self.removebookmarkerbuttons.values():
            self.lowerLeftFloat.remove_widget(k)

        self.bookmarkers = {}
        self.bookmarkerbuttons = {}
        self.removebookmarkerbuttons = {}
        try:
            for i in range(0, len(positions)):
                name = names[i]
                newmarker = MapMarker(lat=positions[i][0], lon=positions[i][1], source="./images/bookmarker.png")
                self.add_marker(newmarker)
                self.bookmarkers[name] = newmarker

                newbookmarkbutton = Button(text=name, size_hint=(0.65, 0.1),
                                           pos_hint={"center_x": 0.4,
                                                     "center_y": 0.65 - (0.1 * len(self.bookmarkerbuttons))})
                newbookmarkbutton.bind(on_release=lambda e, name=name: self.gotoBookmark(name))
                self.lowerLeftFloat.add_widget(newbookmarkbutton)
                self.bookmarkerbuttons[name] = newbookmarkbutton

                newremovebookmarkbutton = Button(text="X", size_hint=(0.2, 0.1),
                                                 pos_hint={"center_x": 0.83,
                                                           "center_y": 0.65 - (
                                                                   0.1 * len(self.bookmarkerbuttons)) + 0.1})
                newremovebookmarkbutton.bind(on_release=lambda e, name=name: self.deleteBookmark(name))
                self.lowerLeftFloat.add_widget(newremovebookmarkbutton)
                self.removebookmarkerbuttons[name] = newremovebookmarkbutton
        except KeyError:
            print("Something went wrong.")

    def on_touch_down(self, touch):
        x = touch.pos[0]
        y = touch.pos[1]
        latlon = self.get_latlon_at(x, y)
        lat = latlon.lat
        lon = latlon.lon

        super().on_touch_down(touch)

        if not self.leftFloat.collide_point(x, y) and not self.lowerLeftFloat.collide_point(x,
                                                                                            y) and not self.settingsButton.collide_point(x,y) and not self.detailsButton.collide_point(x,y) and not touch.is_mouse_scrolling:
            if self.switch.active:
                if self.marker is not None:
                    self.remove_marker(self.marker)
                self.marker = MapMarker(lat=lat, lon=lon, source='./images/marker.png')
                self.add_marker(self.marker)
                self.getWeather(lat, lon)
