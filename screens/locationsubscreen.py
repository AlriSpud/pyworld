from kivy import require
require("2.1.0")

from kivy.core.window import Window
from kivy.uix.popup import Popup
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput

from components.popups import InfoPopupContent

import geocoder

class LocationSubScreen(FloatLayout):
    def __init__(self,mapscreen,forecastscreen):
        super().__init__()
        self.mapscreen,self.forecastscreen = mapscreen,forecastscreen
        self.locationBox = TextInput(pos_hint={"center_x":0.45,"center_y":0.9},size_hint=(0.8,0.1),font_size=0.057*Window.height,multiline=False,on_text_validate=self.submit)
        self.searchButton = Button(text="\ue041",pos_hint={"center_x":0.9,"center_y":0.9},font_size=0.04*Window.height,size_hint=(0.1,0.1),font_name="./dripicons-v2.ttf",on_release=lambda e: self.submit(self.locationBox))
        self.add_widget(self.locationBox)
        self.add_widget(self.searchButton)
    def submit(self,e):
        try:
            g = geocoder.location(e.text)
            self.forecastscreen.setLatLon(g.latlng[0],g.latlng[1])
            self.mapscreen.setLatLon(g.latlng[0], g.latlng[1])
            show = InfoPopupContent(
                "The location has been updated throughout the app.\nIf you don't get the results you were expecting, you\nmay need to be more specific.")
            popupWindow = Popup(title="Done!", content=show, size_hint=(None, None),
                                 size=(self.size[0] * .55, self.size[1] * .35), auto_dismiss=False)
            popupWindow.content.button.bind(on_release=popupWindow.dismiss)
            popupWindow.open()
        except IndexError:
            show = InfoPopupContent(
                "We couldn't find that place.")
            popupWindow = Popup(title="Sorry", content=show, size_hint=(None, None),
                                size=(self.size[0] * .55, self.size[1] * .35), auto_dismiss=False)
            popupWindow.content.button.bind(on_release=popupWindow.dismiss)
            popupWindow.open()
