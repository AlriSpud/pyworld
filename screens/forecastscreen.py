from kivy import require
require("2.1.0")

from kivy.core.window import Window
from kivy.network.urlrequest import UrlRequest
from kivy.uix.scrollview import ScrollView
from kivy.uix.gridlayout import GridLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.tabbedpanel import TabbedPanel, TabbedPanelHeader

from components.forecastsegment import ForecastSegment

from screens.locationsubscreen import LocationSubScreen


class ForecastScreenContent(FloatLayout):
    def __init__(self, app, c_or_f,kph_or_mph,mm_or_in,key):
        super().__init__()
        self.scrollview = ScrollView(size_hint=(1,None),size=(Window.width,Window.height),do_scroll_y=True)
        self.app = app
        self.forecasts = []
        self.lat, self.lon = None,None
        self.data = None
        self.c_or_f = c_or_f
        self.kph_or_mph = kph_or_mph
        self.mm_or_in = mm_or_in
        self.key = key
        self.segmentlayout = GridLayout(size_hint = (1, None),cols=1)
        self.segmentlayout.bind(minimum_height=self.segmentlayout.setter('height'))
        self.locationScreen = LocationSubScreen(self.app.mapview,self)
        for i in range(0,7):
            x = ForecastSegment(i, size_hint=[1, None], height=Window.height * 0.42,c_or_f=c_or_f,mm_or_in=mm_or_in)
            self.forecasts.append(x)
            self.segmentlayout.add_widget(x)
        self.scrollview.add_widget(self.segmentlayout)
        self.mainPanel = TabbedPanel(tab_pos="top_left",width=Window.width,height=Window.height)
        self.mainPanel.do_default_tab = False

        class CustomTabbedPanelHeader(TabbedPanelHeader):
            def __init__(self,screen,**kwargs):
                super().__init__(**kwargs)
                self.screen = screen
            def on_touch_down(self, touch):
                pass
            def on_touch_up(self, touch):
                if self.collide_point(touch.x,touch.y):
                    self.screen.app.screenManager.switch_to(self.screen.app.mapscreen, direction='right')
            def on_press(self):
                pass
            def on_release(self, *largs):
                pass
        self.backtab = CustomTabbedPanelHeader(text="q",screen=self,font_name="./dripicons-v2.ttf")
        self.mainPanel.add_widget(self.backtab)

        self.forecasttab = TabbedPanelHeader(text="Forecast")
        self.forecasttab.content = self.scrollview
        self.mainPanel.add_widget(self.forecasttab)

        self.searchtab = TabbedPanelHeader(text="Search")
        self.searchtab.content = self.locationScreen
        self.mainPanel.add_widget(self.searchtab)

        self.mainPanel.set_def_tab(self.forecasttab)

        self.add_widget(self.mainPanel)
    def setLatLon(self,lat,lon):
        self.lat = lat
        self.lon = lon
        self.getWeather(self.lat,self.lon)
    def updateUnitsData(self,c_or_f,kph_or_mph,mm_or_in,lat,lon):
        self.c_or_f = c_or_f
        self.kph_or_mph = kph_or_mph
        self.mm_or_in = mm_or_in
        if lat is not None and lon is not None:
            self.lat, self.lon = lat, lon
            self.getWeather(lat,lon)
    def getWeather(self, lat, lon):
        def setStuff(req, result):
            self.data = result
            for forecast in self.forecasts:
                forecast.updateData(result,self.c_or_f,self.mm_or_in)

        if self.c_or_f == "c":
            UrlRequest(
                'https://api.weatherbit.io/v2.0/forecast/daily?key=' + self.key + '&lat=' + str(lat) + '&lon=' + str(
                    lon) + "&units=m",
                setStuff)
        else:
            UrlRequest(
                'https://api.weatherbit.io/v2.0/forecast/daily?key=' + self.key + '&lat=' + str(lat) + '&lon=' + str(
                    lon) + "&units=i",
                setStuff)
