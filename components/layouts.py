from kivy import require
require("2.1.0")

from kivy.graphics import RoundedRectangle, Color
from kivy.uix.floatlayout import FloatLayout

class RoundedCornerLayout(FloatLayout):
    def __init__(self, ssize, posaddy, roundedness, colour):
        super().__init__()
        with self.canvas.before:
            rgba = tuple(int(str(colour).strip("#")[i:i+2], 16)/255 for i in (0, 2, 4, 6))
            Color(rgba[0],rgba[1],rgba[2],rgba[3])
            self.rect = RoundedRectangle(
                pos=self.pos,
                size=self.size,
                radius=[(roundedness, roundedness),
                        (roundedness, roundedness),
                        (roundedness, roundedness),
                        (roundedness, roundedness)])

        self.bind(pos=lambda obj, pos: setattr(self.rect, "pos", pos))
        self.bind(size=lambda obj, size: setattr(self.rect, "size", size))
        self.size = ssize
        self.pos = (10, 10 + posaddy)