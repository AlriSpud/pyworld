from kivy import require
require("2.1.0")

from kivy.uix.floatlayout import FloatLayout
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.popup import Popup
from kivy.uix.widget import Widget

import webbrowser

class InfoPopupContent(FloatLayout):
    def __init__(self, text):
        super().__init__()
        self.label = Label(text=text, markup=True, pos_hint={"center_x": 0.45, "center_y": 0.7})
        self.button = Button(text="Okay", size_hint=(0.3, 0.35), pos_hint={"center_x": 0.86, "center_y": 0.17})
        self.add_widget(self.label)
        self.add_widget(self.button)


class TextInputPopupContent(FloatLayout):
    def __init__(self, text, default):
        super().__init__()
        self.label = Label(text=text, pos_hint={"center_x": 0.5, "center_y": 0.8})
        self.textinput = TextInput(text=default, multiline=False, pos_hint={"center_x": 0.5, "center_y": 0.5},
                                   size_hint=(0.7, 0.2))
        self.cancel = Button(text="Cancel", size_hint=(0.3, 0.25), pos_hint={"center_x": 0.86, "center_y": 0.12})
        self.okay = Button(text="Okay", size_hint=(0.3, 0.25), pos_hint={"center_x": 0.53, "center_y": 0.12})
        self.add_widget(self.label)
        self.add_widget(self.textinput)
        self.add_widget(self.okay)
        self.add_widget(self.cancel)


class AboutPopup(Widget):
    def __init__(self, size):
        super().__init__()
        about = \
        """
        [size=18][b]PyWorld v1.0[/b][/size]
        Created using:
            - Kacper Florianski's [color=0099ff][u][ref=https://github.com/TheCodeSummoner/kivy-circular-progress-bar]Kivy Circular Progress Bar[/ref][/u][/color]
            - The open source [color=0099ff][u][ref=https://github.com/kivy/kivy]Kivy[/ref][/u][/color] framework
            - The kivy_garden [color=0099ff][u][ref=https://github.com/kivy-garden/mapview]Mapview[/ref][/u][/color] widget
            - [color=0099ff][u][ref=https://github.com/kivy/buildozer]Buildozer[/ref][/u][/color]'s android build capabilities
            - [color=0099ff][u][ref=https://weatherbit.io]Weatherbit.io[/ref][/u][/color]'s APIs
            - [color=0099ff][u][ref=https://github.com/amitjakhu/dripicons]Dripicons[/ref][/u][/color] V2
        
        Kivy Circular Progress Bar, Kivy, Mapview, and 
        Buildozer are all under the [color=0099ff][u][ref=https://opensource.org/licenses/MIT]MIT License[/ref][/u][/color]
        The weatherbit.io APIs are used under [color=0099ff][u][ref=https://creativecommons.org/licenses/by-nc/4.0/]CC BY-NC 4.0[/ref][/u][/color]
        Dripicons V2 is licensed under  [color=0099ff][u][ref=https://creativecommons.org/licenses/by-sa/4.0/]CC BY-SA 4.0[/ref][/u][/color]
        """
        def refpress(*args):
            webbrowser.open(args[1])
        show = InfoPopupContent(about)
        show.label.bind(on_ref_press=refpress)
        show.label.pos_hint["center_y"] = 0.6
        show.button.size_hint_y = 0.15
        show.button.pos_hint["center_y"] = 0.0725
        popupWindow = Popup(title="About", content=show, size_hint=(None, None),
                            size=size,
                            auto_dismiss=False)
        popupWindow.content.button.bind(on_release=popupWindow.dismiss)
        popupWindow.open()