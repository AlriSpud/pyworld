from kivy import require
require("2.1.0")

from kivy.core.window import Window
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.image import AsyncImage
from kivy.uix.label import Label
from kivy.graphics import Rectangle,Color,Line
from kivy.core.text import Label as GraphicsLabel

from components.circularbar import CircularProgressBar

import datetime

class ForecastSegment(FloatLayout):
    def __init__(self,day,**kwargs):
        super().__init__()
        self.startIndex = 1
        self.height = kwargs['height']
        self.size_hint= kwargs['size_hint']
        self.c_or_f = kwargs['c_or_f']
        self.mm_or_in = kwargs['mm_or_in']
        self.data = None
        self.day = day
        self.rect = None
        with self.canvas.before:
            Color(1,1,1,1)
            self.rect = Line(rectangle=[self.pos[0],self.pos[1],self.pos[0]+self.size[0],self.pos[1]+self.size[1]],width=2)
        self.dayLabel = Label(text="Monday, 13 May 2022",pos_hint={"center_x": 0.25, "center_y":0.87},font_size=Window.height*0.042)
        degreessign = "º?"
        if self.c_or_f == "c":
            degreessign = "ºC"
        elif self.c_or_f == "f":
            degreessign = "ºF"
        self.max = Label(text='[color=ff0000]15'+degreessign+'[/color]', markup=True, pos_hint={"center_x": 0.305, "center_y": 0.63}, font_size=Window.height*0.042)
        self.min = Label(text='[color=0000ff]5'+degreessign+'[/color]', markup = True, pos_hint={"center_x": 0.195, "center_y": 0.63}, font_size=Window.height*0.042)
        self.image = AsyncImage(source="https://www.weatherbit.io/static/img/icons/c04d.png",
                                     pos_hint={"center_x": 0.25, "center_y": 0.4})
        self.desc = Label(text="Partly cloudy",pos_hint={"center_x": 0.25, "center_y":0.2})
        self.rainChanceLabel = Label(text="Chance of Rain: ", pos_hint={"center_x":0.57,"center_y":0.6},font_size=Window.height*0.03)
        self.rainChanceBar = CircularProgressBar(y=self.day*self.height+self.height*0.47,x=Window.width-Window.width*0.3125)
        self.rainChanceBar.label = GraphicsLabel(text="30%",font_size=Window.height * 0.033)
        self.rainChanceBar.widget_size = int(Window.height*0.125)
        self.rainChanceBar.thickness = int(Window.width * 0.00875)
        self.rainChanceBar.max = 100
        self.rainChanceBar.min = 0
        self.rainChanceBar.value = 30
        self.rainAmountLabel = Label(text="Possible precipitation: 55 mm",pos_hint={"center_x":0.63,"center_y":0.35},font_size=Window.height*0.03)
        self.add_widget(self.rainAmountLabel)
        self.add_widget(self.rainChanceLabel)
        self.add_widget(self.rainChanceBar)
        self.add_widget(self.max)
        self.add_widget(self.min)
        self.add_widget(self.desc)
        self.add_widget(self.image)
        self.add_widget(self.dayLabel)

    def on_size(self,instance,size):
        try:
            self.rect.rectangle=[self.pos[0],self.pos[1],self.pos[0]+size[0],self.pos[1]+size[1]]
            self.dayLabel.font_size = size[1] * 0.1
        except AttributeError:
            pass #manual init hasn't happened yet
    def updateData(self,data,c_or_f,mm_or_in):
        degreessign = "º?"
        if c_or_f == "c":
            degreessign = "ºC"
        elif c_or_f == "f":
            degreessign = "ºF"
        self.data = data
        for r in self.data["data"]:
            if r["datetime"] == datetime.date.today().strftime("%Y-%m-%d"):
                self.startIndex = self.data["data"].index(r)
                break

        splitdate = str(self.data["data"][self.startIndex+self.day]["datetime"]).split("-")
        splitdate = [int(d) for d in splitdate]
        dateobj = datetime.date(*splitdate)
        date = dateobj.strftime("%A, %d %B %Y")
        self.dayLabel.text = date
        self.rainChanceBar.label = GraphicsLabel(text=str(self.data["data"][self.startIndex+self.day]["pop"])+"%",font_size=20)
        self.rainChanceBar.value = self.data["data"][self.startIndex+self.day]["pop"]
        self.rainAmountLabel.text = "Possible precipitation: "+ str(round(self.data["data"][self.startIndex+self.day]["precip"],2)) + mm_or_in
        self.desc.text = str(self.data["data"][self.startIndex+self.day]["weather"]["description"])
        self.image.source = "https://www.weatherbit.io/static/img/icons/" + self.data["data"][self.startIndex+self.day]["weather"]["icon"] + ".png"
        self.min.text = "[color=0000ff]" + str(self.data["data"][self.startIndex + self.day]["min_temp"]) + degreessign + "[/color]"
        self.max.text = "[color=ff0000]" + str(self.data["data"][self.startIndex + self.day]["max_temp"]) + degreessign + "[/color]"

