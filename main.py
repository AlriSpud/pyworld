from kivy import require
require("2.1.0")

from kivy.core.window import Window
from kivy.app import App
from kivy.storage.jsonstore import JsonStore
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.settings import Settings
from kivy.uix.image import Image

from components.layouts import RoundedCornerLayout

from screens.mapscreen import MapScreenContent
from screens.forecastscreen import ForecastScreenContent

settings_path = "."
try:
    from android.storage import app_storage_path
    settings_path = app_storage_path()
except ModuleNotFoundError:
    print("You aren't running this on android, so for now the storage path is ./")
store = JsonStore(settings_path + "/appdata.json")
weatherbitkey = "5cfef25f57ab4b6a895fe598c17bd62b"
c_or_f = "c"
kph_or_mph = "kph"
mm_or_in = "mm"
colourOfStuff = 0x000000ff
colourOfSecondary = 0xffffffff

class PyWorld(App):
    def __init__(self):
        super().__init__()
        self.forecast = None
        self.forecastscreen = None
        self.settingsButton = None
        self.screenManager = None
        self.mapscreen = None
        self.mapview = None

    def build(self):
        global c_or_f
        global kph_or_mph
        global mm_or_in
        global colourOfStuff
        try:
            from android.permissions import request_permissions, Permission
            request_permissions([Permission.INTERNET])
        except ModuleNotFoundError:
            print("It seems you aren't running this on android")

        value = self.config.get('Main', 'units')
        if value == "metric":
            c_or_f = "c"
            kph_or_mph = "kph"
            mm_or_in = "mm"
        elif value == "imperial":
            c_or_f = "f"
            kph_or_mph = "mph"
            mm_or_in = "in"
        else:
            c_or_f = "??"
            kph_or_mph = "??"
            mm_or_in = "??"
        colour = self.config.get('Main', 'colour')
        colourOfStuff = colour
        colourOfSecondary = self.config.get('Main','secondaryColour')
        self.screenManager = ScreenManager()

        self.mapscreen = Screen(name="MapScreen")
        self.mapview = MapScreenContent(zoom=1, app=self, c_or_f=c_or_f, store=store,key=weatherbitkey,colour=colourOfStuff,secondaryColour=colourOfSecondary)

        self.forecastscreen = Screen(name="ForecastScreen")
        self.forecast = ForecastScreenContent(self,c_or_f,kph_or_mph,mm_or_in,weatherbitkey)

        self.settings_cls = Settings
        self.use_kivy_settings = False

        self.icon = "./images/PotatoWeatherMap.png"

        self.mapscreen.add_widget(self.mapview)
        self.forecastscreen.add_widget(self.forecast)
        self.screenManager.add_widget(self.mapscreen)
        self.screenManager.add_widget(self.forecastscreen)
        Window.bind(on_keyboard=self.hook_keyboard)
        return self.screenManager

    def hook_keyboard(self, window, key, *largs):
        if key == 27:
            self.screenManager.switch_to(self.mapscreen, direction='right')
            return True

    def build_config(self, config):
        config.setdefaults('Main', {
            'units': 'metric',
            'colour': "#000000FF",
            'secondarycolour': "#FFFFFFFF"
        })

    def build_settings(self, settings):
        settingjson = open('settingpanel.json').read()
        settings.add_json_panel('Weather',
                                self.config, data=settingjson)

    def on_config_change(self, config, section, key, value):
        global c_or_f
        global kph_or_mph
        global colourOfStuff, colourOfSecondary
        global mm_or_in
        if config is self.config:
            token = (section, key)
            if token == ('Main','units') or token == ('Main','colour') or token == ('Main','secondarycolour'):
                if token == ('Main', 'units'):
                    if value == "metric":
                        c_or_f = "c"
                        kph_or_mph = "kph"
                        mm_or_in = "mm"
                    elif value == "imperial":
                        c_or_f = "f"
                        kph_or_mph = "mph"
                        mm_or_in = "in"
                    else:
                        c_or_f = "??"
                        kph_or_mph = "??"
                        mm_or_in = "??"
                elif token == ('Main', 'colour'):
                    colourOfStuff = value
                    for child in self.mapview.children:
                        if type(child) == RoundedCornerLayout or type(child) == Image:
                            self.mapview.remove_widget(child)
                elif token == ('Main', 'secondaryColour'):
                    colourOfSecondary = value
                    for child in self.mapview.children:
                        if type(child) == RoundedCornerLayout or type(child) == Image:
                            self.mapview.remove_widget(child)
                self.mapview.setColourUnits(colour=colourOfStuff,c_or_f=c_or_f,secondaryColour=colourOfSecondary)
                self.forecast.updateUnitsData(c_or_f,kph_or_mph,mm_or_in,None,None)
                positions = [(value.lat, value.lon) for value in self.mapview.bookmarkers.values()]
                names = [value.text for value in self.mapview.bookmarkerbuttons.values()]
                self.mapview.refresh(positions,names)

    def on_stop(self):
        super().on_stop()
        positions = []
        for val in self.mapview.bookmarkers.values():
            positions.append((val.lat, val.lon))
        names = []
        for val in self.mapview.bookmarkerbuttons.values():
            names.append(val.text)
        store.put("bookmarkmarkerspos", val=positions)
        store.put("bookmarknames", val=names)
    def on_pause(self):
        super().on_stop()
        positions = []
        for val in self.mapview.bookmarkers.values():
            positions.append((val.lat, val.lon))
        names = []
        for val in self.mapview.bookmarkerbuttons.values():
            names.append(val.text)
        store.put("bookmarkmarkerspos", val=positions)
        store.put("bookmarknames", val=names)
PyWorld().run()
